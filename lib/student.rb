class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  # def course_factory
  #   Course.new
  # end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    raise StandardError if has_conflict?(new_course)
    @courses.push(new_course).uniq!
    new_course.students.push(self)
    # $stderr.puts @courses
    # $stderr.puts new_course
  end

  def has_conflict?(new_course)
    @courses.any? do |course| course.conflicts_with?(new_course)
    end
  end


  def course_load
    credit_hash = Hash.new(0)
    @courses.each do |course|
      credit_hash[course.department] += course.credits
    end
    credit_hash
  end

end
